package org.hibernate.userguide.model;

/**
 * @author Vlad Mihalcea
 */
//tag::hql-examples-domain-model-example[]
public enum PhoneType {
    LAND_LINE,
    MOBILE;
}
//end::hql-examples-domain-model-example[]
