package org.hibernate.userguide.model;

/**
 * @author Vlad Mihalcea
 */
//tag::hql-examples-domain-model-example[]
public enum AddressType {
    HOME,
    OFFICE
}
//end::hql-examples-domain-model-example[]
