package org.hibernate.userguide.model;

import javax.persistence.Entity;

/**
 * @author Vlad Mihalcea
 */
//tag::hql-examples-domain-model-example[]
@Entity
public class WireTransferPayment extends Payment {
}
//end::hql-examples-domain-model-example[]
